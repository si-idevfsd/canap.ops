# Deployment of the application for EPFL apprentices applications

## How it works in a nutshell ?

Canap is a set of two web applications:
 1. **Form** is the public web site allowing candidate to apply for an apprenticeship position at EPFL. 
 2. **Gest** is the backoffice where trainers can evaluate the apprentices.

Both applications are served through a reverse web proxy (_Traefik_) which takes care of the encrypted communication with the user browser as depicted in the following schema:

```mermaid
flowchart TD
    Z(browser) <--> A(Traefik)
    A <--> B(Form)
    A <--> C(Gest)
    C <--> D(API)
    B <--> E(DB)
    D <--> E
```

Data is stored in a database which is not managed by this configuration as a code except in the case of the test server (see below).

In the current configuration, all the blocks composing the applications are deployed on the same virtual machine as individual docker containers.

### The 4 main blocks
#### Form
Form allows apprentices to apply for the positions currently available at the EPFL. They can also modify their postulation.
* **stack**: is a full stack application written in PHP

#### Gest
Gest can be used to manage applications. It has two views:
    1. The responsible view, which allows to validate or reject postulations
    2. The trainer view, where trainers can make their choices for their new apprentices.
* **stack**: Gest is a frontend application in JavaScript using vue.js and babel.js

#### API
API enables communication between Gest and the database, the storage of applicant documents and the login.
* **stack**: API is a backend application written in PHP using Laravel/Lumen framework

#### Traefik
[Traefik](https://github.com/traefik/traefik) is the HTTP reverse proxy exposing the two applications.
 * **configuration** dynamic via docker labels
 * **SSL certificates** generated with [let's encrypt](https://letsencrypt.org/) certbot for the public production server and provided as static files in the other cases.
 * **dashboard** exposed on the virtual machine internal name (e.g. http://itsidevfsd0025.epfl.ch/dashboard)

### How the container images are built
1. Git pull from the prod branch into `/srv/canap/src`;
1. Copy the code from `/srv/canap/src` to `/srv/build`;
1. Copy the docker files from `/srv/canap/src` to `/srv/build`;
1. Write relevant configuration files (_e.g._ Apache or Traefik static configuration) to build directory;
1. Build locally the container image from `/srv/build`.

---

## On which server does it run?

Currently we have 3 VM dedicated to the different environments of the application: production, staging, test.

### Production 

The production server is the only one exposed to the internet

* **VM**:
    * **Name**: itsidevfsd0025
    * **IO**: 128.178.218.242
    * **Network zone**: DMZ
    * **Open ports**: 80,443,587
    * **OS**: Ubuntu version 22.04
* **URL**: https://canap.epfl.ch
* **DB**: `db-scx-8h.epfl.ch` (`10.95.80.113`) centrally managed by ITOP-MWS 
* **canapsible switch**: `--prod`

### Staging

Is the server used to expose new features to the administrative users. It is not accessible from outside the EPFL network.

* **VM**:
    * **Name**: `itsidevfsd0020`
    * **IP**: `10.95.81.30`
    * **Network zone**: production
    * **OS**: Ubuntu version 22.04
* **URL**: https://canap-new.epfl.ch
* **DB**: `db-scx-test.epfl.ch` (`10.95.96.50`)
* **canapsible switch**: `--test` (will be changed to `--staging`)

### Test

Is a server for developer's only to check for possible inconsistencies wrt the local development environment

* **VM**:
    * **Name**: `itsidevfsd0019`
    * **IP**: `10.95.96.134`
* **URL**: https://canap.fsd.team
* **DB**: local container `canap_db` on the docker network;
* **canapsible switch**: `--dev` (will be changed to `--test`)

---

## How is it deployed? 

The application is deployed using Ansible configuration as a code using a wrapper script named `canapsible` which is an instance of the `Xsible` family of scripts based on the ISAS-FSD [Ansible suitcase](https://github.com/epfl-si/ansible.suitcase).

What follows is a detailed description of all the parameters and environment variables used to configure the application.

### 5 containers docker 
* Form 
* Gest
* API
* Traefik
* DB (only if exec_env = test)

### Form
* URL:
    * **Prod**: canap.epfl.ch
    * **Staging**: canap-new.epfl.ch
    * **Test**: canap.fsd.team


* **Path**:

|      Description       |      VM path      | VM path (build directory) |       Container path        |          Code path          |                   Var file                   |
|:----------------------:|:-----------------:|:-------------------------:|:---------------------------:|:---------------------------:|:--------------------------------------------:|
|  Code  | `/srv/canap/Formulaire` |  `/srv/canap/build/form`  |      `/var/www/html/`       |        `/Formulaire`        | `canap.ops/roles/canap/vars/canap_vars.yml` |
| PassEnv var config |         -         |             -             | `/etc/apache2/conf-enabled` | `/Docker/canap_apache.conf` |                      -                       |
| Mail config|   -     |        `/srv/canap/build/etc/msmtprc`                        |                         -    |          -                   |                                        -      |

* **Vars**:
    * **`ENVIRONMENT`**: Execution environement
    * **`SENDEMAILS`**: Set if we send mail to the applicant and/or the head of apprenticeships
    * **`MAIL_FROM`**: Email adress that sends emails to different canAp users
    * **`EMAIL_FORMATION`**: Email adress of head of apprenticeships 
    * **`MAIL_CONTACT`**: Email adress that's used to notify head of apprenticeships when there is a new application. 
    * **`MAIL_REPLYTO`**: If you reply to the email sent by no-reply, the email will be sent to this address
    * **`FILESERVERPATH`**: File path where the application documents can be found
    * **Connection to the DB**:
        * `DB_CONNECTION`
        * `DB_PASSWORD`
        * `DB_HOST`
        * `DB_PORT`
        * `DB_DATABASE`
        * `DB_USERNAME`

* **Access**:
    * Database
    * Mail server

### Gest
* URL:
    * Prod:  canap.epfl.ch/gest
    * Staging: canap-new.epfl.ch/gest
    * Test: canap.fsd.team/gest

* **Path**:

|      Description       |      VM path      | VM path (build directory) |       Container path        |          Code path          |                   Var file                   |
|:----------------------:|:-----------------:|:-------------------------:|:---------------------------:|:---------------------------:|:--------------------------------------------:|
|  Code  | `/srv/canap/Gestion` |  `/srv/canap/build/gest`  |      `/home/node/app`       |        `/Gestion`        | `canap.ops/roles/canap/vars/canap_vars.yml` |
| Set the `Api_location` and the `login_redirect` | `/srv/canap/src/Gestion/index.js` & `/src/config/index.js` |  `/srv/canap/build/gest/src/config/index.js` | - | `/Gestion/src/config/index.js` | `canap.ops/roles/canap/vars/canap_vars.yml` |
| Traefik route config for gest |`/srv/canap/Gestion/vue.config.js` | `/srv/canap/build/vue.config.js` & `/gest/vue.config.js` | - | `/Gestion/vue.config.js`                   |                                        -      |
* **Vars**
    * `NODE_ENV`: Node server environment
    * `API_LOCATION`: URL used to access the API
    * `LOGIN_REDIRECT`: URL used to redirect to the API login route

* Access:
    * DB
    * Mail server
    * API

### API
* URL:
    * Prod: canap.epfl.ch/api
    * Staging: canap-new.epfl.ch/api
    * Test: canap.fsd.team/api

* **Path**:

|      Description       |      VM path      | VM path (build directory) |       Container path        |          Code path          |                   Var file                   |
|:----------------------:|:-----------------:|:-------------------------:|:---------------------------:|:---------------------------:|:--------------------------------------------:|
|  Code  | `/srv/canap/API` |  `/srv/canap/build/API`  |      `/var/www/html/`       |        `/API`        | `canap.ops/roles/canap/vars/canap_vars.yml` |

* **Vars**
    * **`FILESERVERPATH`**: File path where the application documents can be found
    * **`SCIPERDEVRESPONSABLE`**:  Scipers (list) for the developers, which provides access to the view responsible for the "gest" service
    * **`SCIPERSUPERUSER`**: Sciper for developers, which gives you rights to view all branches of apprenticeships in the "postulations" view of the "gest" service.
    * **`TEQUILA_REQUIRE_PARAMS`**: Group that defines who has access to the "gest" service
    * **`TEQUILA_RETURN_URL`**: URL that allows the user to return to the "gest" service once connected with Tequila
    * **`JWT_SECRET`**: Secret JWT
    * **Connection to the DB**:
        * `DB_CONNECTION`
        * `DB_PASSWORD`
        * `DB_HOST`
        * `DB_PORT`
        * `DB_DATABASE`
        * `DB_USERNAME`

* Access:
    * DB
    * Gest

### Traefik

* **Path**:

| Description |        VM path        | OPS path | Container path |                     Var file                     |
|:-----------:|:---------------------:|:--------:|:--------------:|:------------------------------------------------:|
|    Code     | `/srv/traefik/`    |    -     | `/etc/traefik` | `canap.ops/roles/traefik/vars/traefik_vars.yml` |
|   Config    | `/srv/traefik/config/*` |    `canap.ops/roles/traefik/templates/*.yml` &   `canap.ops/roles/traefik/templates/traefik.yml`  | `/etc/traefik` | `canap.ops/roles/traefik/vars/traefik_vars.yml` |

### Config details (only in the ops repos):
* `canap.ops/roles/canap/templates/traefik.yml` → Configuration of the Traefik routes
#### Redirection from canap-gest.epfl.ch to canap.epfl.ch/gest
* `canap.ops/roles/canap/templates/gest_redirect_index.html` 
* `canap.ops/roles/canap/templates/traefik-gest.yml`
* `canap.ops/roles/canap/templates/gest_redirect_index.html`

* Access:
    * Form
    * Gest
    * API

* Used Ports:
    * 80
    * 443

### DB (ONLY IN DEV)
* **Path**:

| Description |     VM path     | VM path (build directory) |  Container path  | Code path  |                   Var file                   |
|:-----------:|:---------------:|:-------------------------:|:----------------:|:----------:|:--------------------------------------------:|
|    code     | `/srv/canap/db` |             -             | `/var/lib/mysql` | `/data/DB` | `canap.ops/roles/canap/vars/canap_vars.yml` |

* **Vars**
    * **Connection to the DB**:
        * `DB_CONNECTION`
        * `DB_PASSWORD`
        * `DB_HOST`
        * `DB_PORT`
        * `DB_DATABASE`
        * `DB_USERNAME`
* Access:
    * Form
    * API

* Used network:
    * canap

---

## Pre-requisites
1. Clone this repos
2. The operator must be a member of the epfl_canap Keybase group.
    1. Install your SSH key in: `/keybase/team/epfl_canap/secrets.yml` by inserting your github login in the list.
    1. If you don't have a github account, please create one and link an SSH key to your account.
    1. Ask the current operator to run the "canapsible" script so that your SSH key can be read by the program.
    1. Finally, test your SSH connection by writing to your command console: `ssh -l root 10.95.96.134`
    1. install your SSH key in: `/keybase/team/epfl_canap/secrets.yml` by inserting your github username in the list.
3. The operator must have his SSH key in the ISAS-FSD bastion-prod and bastion-test


## How to use it ?

## Prod
1. Change the environnement of the task to **prod**: "Ensure form containers is running" in the `roles/canap/task/run.yml` (will be 12 factored)
2. Open a console in the ops project
3. Run the Ansible script in the terminal: `./canapsible --prod`

## Staging
1. Change the environnement of the task to **test**: "Ensure form containers is running" in the `roles/canap/task/run.yml` (will be 12 factored)
2. Open a console in the ops project
3. Run the Ansible script in the terminal: `./canapsible --test`

*If you don't want to build the Gest application you can set the "`isbuild`" to false in the hosts file.*  
*If you want to have debugs information set the "`debug`" to true in the host file.*

## Other command
Re-build containers
`./canapsible -t api.reload,gest.reload,form.reload,db.reload,traefik.reload`

installing canAp
`./canapsible -t canap.install`

Installing the canApp config
`./canapsible -t canap.config`

Start the container build
`./canapsible -t canap.run`
